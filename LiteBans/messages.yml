#
# The following variables can be used in most messages that involve a punishment:
#
# Punishment specific variables
# $id - The ID of the punishment in the database.
# $type - Type of punishment - ban, mute, warn, kick.
# $reason - the reason for the punishment
# $executor - the moderator's name, or their display name (/nick) if this is enabled in the configuration
# $executorUUID - the moderator's UUID
# $permanent - whether this punishment is permanent
# $ipban - whether this is an IP-ban
# $silent - whether this punishment was executed silently (-s)
# $active - whether this punishment is active
#
# Affected player specific variables
# $playerDisplayName - player display name. If display names are not enabled in the configuration or if the player's display name is not available in the message's context, the player's regular name will be used instead.
# $playerName - player name
# $playerUUID - UUID of affected player
# $playerIP - IP of affected player
# $geoip - Country of affected player, requires GeoIP to be enabled in the configuration, won't work with imported bans
#
# Servers
# These variables represent a server.
# If the plugin is installed on Spigot, a server is represented by the "server_name" option in config.yml.
# If the plugin is installed on BungeeCord, a server is represented by their name in the "servers" section in the proxy's config.yml.
# $serverScope - the scope of the punishment (the server(s) it will affect)
# $serverOrigin - the origin of the punishment (the server/subserver it was placed on)
#
# Dates
# Example format: "2017-02-03", depends on time_format
# $dateStart - date the punishment was placed
# $dateEnd - date the punishment will expire, "forever" if permanent
#
# Durations
# Example format: "20 days, 5 hours, 2 minutes". If permanent, "forever", if expired, "expired" (both are configurable).
# $duration - time until expiry
# $originalDuration - the full duration of the punishment.
# $timeSince - time since placement
#
# Placeholders
# These variables represent messages in this configuration (messages.yml)
# $base - banned_message_base
# $appealMessage - banned_message_appeal_message
#
# Global variables
# $activeBans, $activeMutes, $activeWarnings - total number of global active punishments
# $totalBans, $totalMutes, $totalWarnings - total number of global punishments (including inactive ones)
#
# Vault-specific variables
# $playerPrefix - Vault chat prefix of affected player
# $playerSuffix - Vault chat suffix of affected player
# $executorPrefix - Vault chat prefix of executor
# $executorSuffix - Vault chat suffix of executor
# Any message can be disabled by setting it to "". Empty messages will not be sent by the plugin.
#
# JSON examples:
# broadcast_ban: '&e$bannedPlayer &chas been banned. {hoverText: &aHover text here!}'
# Hover text requires "litebans.json.hover_text" to view, players without this permission will see messages without hover text.
#
# https://docs.oracle.com/javase/tutorial/i18n/format/simpleDateFormat.html
# Example time format with hours + minutes:
# time_format: 'dd/MM/yyyy HH:mm'
time_format: yyyy-MM-dd
history_time_format: yyyy-MM-dd
banned_message_base: |
  &c&lLiteBans
  &fByl jsi &bzabanován&f!

  &eInformace:
  &8&l■ &7$dateStart &8&l■
  &8&l■ &b$executor &8&l■
  &8&l■ &f$reason &8&l■
banned_message: |-
  $base
  &8&l■ &7$duration &8&l■

  $appealMessage
banned_message_permanent: |-
  $base
  &8&l■ &cTrvalé zabanování &8&l■

  $appealMessage
banned_message_appeal_message: |
  &eŽádost o Odbanování
  &bmc.LiteBans.cz/unban
  &eTato zpráva může sloužit jako důkaz o zabanování.
banned_message_geoip_blacklist: |-
  &c&lLiteBans
  &fYou are &bbanned&f!

  &8&l■ &7Your location is blacklisted: &f$geoip &8&l■
bungee_switch_banned: |-
  &c&lLiteBans
  &fByl jsi &bzabanován&f!

  &eInformace:
  &8&l■ &f$reason &8&l■
  &8&l■ &b$serverScope &8&l■
default_ban_reason: Porušení pravidel
default_mute_reason: ''
permission_error: '&cLiteBans &8● &fNemáš dostatečné &bpravomoce&f!'
muted: '&cLiteBans &8● &fJsi &bumlčený&f! &8(&a$duration&8)'
muted_permanent: '&cLiteBans &8● &fJsi &bumlčený&f! &8(&aTrvale&8)'
notify:
  banned_player_join: '&4&lAT &c$player se pokusil připojit, ale je zabanovaný ($duration)'
  banned_geoip_blacklist: '&4&lAT &c$player se pokusil připojit, ale je na černé listině
    ($geoip)'
error_no_reason_provided: '&cLiteBans &8● &fMusíš uvést &bdůvod&f!'
error_no_sql_connection: '&cLiteBans &8● &bLiteBans &fnení připojeno k databázi!'
error_no_uuid_found: '&cLiteBans &8● &bHráč &fneexistuje!'
error_console_only: '&cLiteBans &8● &fTento &bpříkaz &fmůže být použit pouze z
  &bkonzole&f!'
internal_error: '&cLiteBans &8● &cInternal Error'
duration_limit_error: '&cLiteBans &8● &bMaximální &fpovolená délka je &b$duration&f!'
warned_join: '&eLiteBans &8● &fTvé nové &bvarování&f:'
warned_join_entry: |-
  &7 - Varoval &b$executor&7: &b$reason
     &8(&apřed $timeSince&8)
command:
  ban:
    usage: '&cLiteBans &8● &fPoužití: &b$command &8[&a-s&8] &8<&ahráč&8> &8[&ačas&8]
      &8[&adůvod&8]'
    unban_usage: '&cLiteBans &8● &fPoužití: &b$command &8<&ahráč&8>'
    example: '&cLiteBans &8● &fUkázka: &b$command &aSkopy 7d Vzpoura'
    silent_prefix: '&8[&aTiché&8] '
    broadcast_ban: '&dLiteBans &8● &a$executor &7zabanoval&8(&bTRVALE&8) &a$bannedPlayer
      &8&l■ &a$reason'
    broadcast_tempban: '&dLiteBans &8● &a$executor &7zabanoval&8(&bDOČASNĚ&8) &a$bannedPlayer
      &8&l■ $tempDuration &8&l■ &a$reason&f'
    broadcast_ip_ban: '&dLiteBans &8● &a$executor &7zabanoval&8(&bIP-TRVALE&8)
      &a$bannedIP &8&l■ &a$reason'
    broadcast_temp_ip_ban: '&dLiteBans &8● &a$executor &7zabanoval&8(&bIP-TRVALE&8)
      &a$bannedIP &8&l■ $tempDuration &8&l■ &a$reason&f'
    broadcast_unban: '&dLiteBans &8● &a$executor &7odbanoval &a$playerName'
    previous_ban_removed: '&aLiteBans &8● &fPředchozí ban pro &b$bannedPlayer &fodebrán.'
    previous_ban_existing: '&cLiteBans &8● &b$bannedPlayer &fje zabanovaný.'
    unban_queue: '&eLiteBans &8● &fPokud se pokusí připojit, budou &bodbanováni&f.'
    error_no_spec: '&cLiteBans &8● &bNeplatná &fspecifikace &bčasu&f!'
    unban_fail: '&cLiteBans &8● &bCíl &fnení zabanovaný!'
    no_override: '&cLiteBans &8● &fHráč již je &bzabanovaný&f!'
    exempt: '&cLiteBans &8● &fNemůžeš zabanovat &b$player&f!'
    cooldown: '&cLiteBans &8● &fMusíš &bpočkat&f, před použitím tohoto příkazu.
      &8(&a$duration&8)'
    response: ''
  mute:
    usage: '&cLiteBans &8● &fPoužití: &b$command &8[&a-s&8] &8<&ahráč&8> &8[&ačas&8]
      &8[&adůvod&8]'
    unmute_usage: '&cLiteBans &8● &fPoužití: &b$command &8<&ahráč&8>'
    example: '&cLiteBans &8● &fUkázka: &b$command &aSkopy 7d Spamování'
    broadcast: '&dLiteBans &8● &a$executor &7umlčel&8(&bTRVALE&8) &a$mutedPlayer
      &8&l■ &a$reason&f'
    broadcast_tempmute: '&dLiteBans &8● &a$executor &7umlčel&8(&bDOČASNĚ&8) &a$mutedPlayer
      &8&l■ &a$tempDuration &8&l■ &a$reason&f'
    broadcast_ip_mute: '&dLiteBans &8● &a$executor &7umlčel&8(&bIP-TRVALE&8) &a$IP-muted
      &8&l■ &a$reason&f'
    broadcast_temp_ip_mute: '&dLiteBans &8● &a$executor &7umlčel&8(&bIP-DOČASNĚ&8)
      &a$IP-muted &8&l■ &a$tempDuration &8&l■ &a$reason&f'
    message: |-
      &c&lLiteBans &fByl jsi &bumlčen&f!
      &eInformace:
      &8&l■ &b$duration
    message_permanent: |-
      &c&lLiteBans &fByl jsi &bumlčen&f!
      &eInformace:
      &8&l■ &bTrvale
    broadcast_unmute: '&dLiteBans &8● &a$executor &7odmlčel &a$bannedPlayer'
    unmute_fail: '&cLiteBans &8● &bCíl &fnení umlčený!'
    no_override: '&cLiteBans &8● &bCíl &fje umlčený!'
    previous_mute_removed: '&aLiteBans &8● &fPředchozí &bumlčení &fhráče odebráno.'
    previous_mute_existing: '&cLiteBans &8● &b$mutedPlayer &fje již umlčený!'
    exempt: '&cLiteBans &8● &fNemůžeš umlčet &b$player&f!'
    notification: '&4&lAT &c$mutedPlayer se snažil psát, ale je umlčený.'
    error_not_enabled: '&cLiteBans &8● &bUmlčení &fnení povoleno!'
    response: ''
  warn:
    usage: '&cLiteBans &8● &fPoužití: &b$command &8[&a-s&8] &8<&ahráč&8> &8[&adůvod&8]'
    unwarn_usage: '&cLiteBans &8● &fPoužití: &b$command &8<&aplayer&8>'
    example: ''
    broadcast: '&dLiteBans &8● &a$executor &7varoval &a$warnedPlayer &8&l■ &a$reason'
    message: |-
      &c&lLiteBans &fByl jsi &bvarován&f!
      &eInformace:
      &8&l■ &b$reason
    list_entry: '&8&l■ &a$executor&7: &8''&a$reason&8'''
    unwarn_response: '&aLiteBans &8● &fPoslední varování hráče &b$player &fbylo
      odstraněno.'
    unwarn_fail: '&cLiteBans &8● &bCíl &fnemá žádné varování.'
    exempt: '&cLiteBans &8● &bNemůžeš &fvarovat &b$player&f!'
    cooldown: '&cLiteBans &8● &fMusíš počkat před použitím! &8(&a$duration&8)'
    response: ''
  history:
    usage: '&cLiteBans &8● &fPoužití: &b$command &8<&ahráč&8> &8[&azáznamy=10&8]'
    start: '&eHistorie &e$target &8(&aLimit: &o$limit&8)&e:'
    ban_entry: |-
      &ePřed &o$timeSince
      &c&lBAN &b$name &8&l► &f$executor &8&l■ &f$reason
    mute_entry: |-
      &ePřed &o$timeSince
      &c&lMUTE &b$name &8&l► &f$executor &8&l■ &f$reason
    warn_entry: |-
      &ePřed &o$timeSince
      &c&lWARN &b$name &8&l► &f$executor &8&l■ &f$reason
    kick_entry: |-
      &ePřed &o$timeSince
      &c&lKICK &b$name &8&l► &f$executor &8&l■ &f$reason
    unban_entry: |2-

      &a&lUNBAN &b$name &8&l► &f$executor
    unmute_entry: |2-

       &a&lUNBAN &b$name &8&l► &f$executor
    active_suffix: ' &a&l●'
    expired_suffix: ' &c&l●'
    active_suffix_temp: |-
      &f &a&l●
      &8(&a$duration&8)
    error_no_history: '&cLiteBans &8● &fŽádná &bhistorie&f.'
  warnings:
    usage: '&cLiteBans &8● &fPoužití: &b$command &8<&ahráč&8>'
    start: '&eAktivní varování pro &o$target&e:'
  prunehistory:
    usage: '&cLiteBans &8● &fPoužití: &b$command &8<&ahráč&8> [&ačas&8]'
    message: '&aLiteBans &8● &fHistorie &bpromazána&f.'
  staffhistory:
    usage: '&cLiteBans &8● &fPoužití: &b$command &8<&ahráč&8> &8[&azáznamy=10]'
    start: '&eHistorie &e$target &8(&aLimit: &o$limit&8)&e:'
  staffrollback:
    usage: '&cLiteBans &8● &b$command &8<&ahráč&8> &8[&ačas&8]'
    message: '&aLiteBans &8● &fVrácení změn bylo úspěšně. &8(&a$amount'
  banlist:
    start: '&8⋘ &a$page ︱ &a$total &8⋙'
  iphistory:
    usage: '&cLiteBans &8● &fPoužití: &b$command &8<&ahráč&8> &8[&azáznamy=10]'
    start: '&eHistorie přihlášení &e$target &8(&aLimit: &o$limit&8)&e:'
    entry: |-
      &ePřed &o$date
      &c&lBAN &b$name &8&l► &f$ip
    error_no_history: '&cLiteBans &8● &fŽádná &bhistorie&f.'
  dupeip:
    usage: '&cLiteBans &8● &fPoužití: &b$command &8<&ahráč&8>'
    start: '&eLiteBans &8● &fSkenuji &b$name&f pro &b$ip&f. &a&l● &7&l● &c&l●'
    start_no_ip: '&eLiteBans &8● &fSkenuji &b$name&f pro&f. &a&l● &7&l● &c&l●'
    separator: '&f, '
    online: '&a'
    offline: '&7'
    banned: '&c'
    muted: '&6'
    multiple_addresses: '[$num adresy]'
  ipreport:
    start: '&eLiteBans &8● &fSkenuji &b$num&f hráčů. &a&l● &7&l● &c&l●'
    entry: '&b$player&f: $result'
  checkban:
    usage: '&cLiteBans &8● &fPoužití: &b$command &8<&ahráč&8>'
    no_ban: '&cLiteBans &8● &bCíl &fnení zabanovaný!'
    banned: |-
      &c&lLiteBans
      &bZabanován

      &eInformace:
      &8&l■ &7$dateStart &f> &7$dateEnd
      &8&l■ &b$executor
      &8&l■ &f$reason

      &8&l■ &7Server: &6$serverOrigin &f* &6$serverScope

      &8&l■ &fIP-Ban $ipban
      &8&l■ &fTichý $silent
      &8&l■ &fTrvalý $permanent
  checkmute:
    usage: '&cLiteBans &8● &fPoužití: &b$command &8<&ahráč&8>'
    no_mute: '&cLiteBans &8● &bCíl &fnení umlčený!'
    muted: |-
      &c&lLiteBans
      &bUmlčený

      &eInformace:
      &8&l■ &7$dateStart &f> &7$dateEnd
      &8&l■ &b$executor
      &8&l■ &f$reason

      &8&l■ &7Server: &6$serverOrigin &f* &6$serverScope

      &8&l■ &fIP-Ban $ipban
      &8&l■ &fTichý $silent
      &8&l■ &fTrvalý $permanent
  lastuuid:
    usage: '&cLiteBans &8● &fPoužití: &b$command &8<&ahráč&8|&aIP&8>'
    message: '&b$name &8&l► &f$uuid'
  geoip:
    usage: '&cLiteBans &8● &fPoužití: &b$command &8<&ahráč&8|&aIP&8>'
    message: '&b$target &8&l► &f$result'
    error_disabled: '&cLiteBans &8● &bGEO-IP &fpodpora není aktivní.'
    error_unavailable: '&cLiteBans &8● &fPodpora &bGEO-IP &fnení v aktuální chvíli
      dostupná. Je &bstažena&f?'
    error_not_found: '&b$target &8&l► &fNenalezeno'
  lockdown:
    usage: '&cLiteBans &8● &fPoužití: &b$command &8<&adůvod&8|&aend&8>'
    message: '&eLiteBans &8● &fServerové &buzavření &fje aktivní. &8(&a$reason&8)'
    stopped: '&eLiteBans &8● &fServerové &buzavření &fje vypnuté.'
    kick_message: |-
      &c&lLiteBans
      &fServer je &buzavřen&f.

      &eInformace:
      &8&l■ &f$reason &8&l■
    kick_message_global: |-
      &c&lLiteBans
      &fServer je &buzavřen&f.

      &eInformace:
      &8&l■ &f$reason &8&l■
    error_not_active: '&eLiteBans &8● &fUzavření není &baktivní&f!'
  kick:
    usage: '&cLiteBans &8● &fPoužití: &b$command &8<&ahráč&8> &8[&adůvod&8]'
    no_match: '&cLiteBans &8● &bHráč &fnenalezen.'
    kick_requested: '&cLiteBans &8● &b$player &fnení na serveru. Aktivuje &bmezi-serverový
      &fpožadavek.'
    message: |-
      &c&lLiteBans
      &fByl jsi &bvyhozen&f!

      &eInformace:
      &8&l■ &b$executor &8&l■
      &8&l■ &f$reason &8&l■
    message_no_reason: |-
      &c&lLiteBans
      &fByl jsi &bvyhozen&f!

      &eInformace:
      &8&l■ &b$executor &8&l■
    response: '&dLiteBans &8● &a$player &7byl vyhozen.'
    broadcast: '&dLiteBans &8● &a$executor &7vyhodil &a$player &8&l■ &a$reason'
    broadcast_no_reason: '&dLiteBans &8● &a$executor &7vyhodil &a$player'
    exempt: '&cLiteBans &8● &fNemůžeš vyhodit &b$player&f!'
  togglechat:
    toggle_off: '&aLiteBans &8● &fChat &bvypnut&f.'
    toggle_on: '&aLiteBans &8● &fChat &bzapnut&f.'
  clearchat:
    broadcast: '&eLiteBans &8● &b$executor &fvymazal chat.'
  mutechat:
    response: '&cLiteBans &8● &fNemůžeš &bpsát&f! Chat je &bdeaktivován&f.'
    broadcast_disabled: '&eLiteBans &8● &fChat &bdeaktivován&f.'
    broadcast_enabled: '&eLiteBans &8● &fChat &baktivován&f.'
  litebans:
    reload_success: '&aLiteBans reloaded successfully.'
    reload_fail_connect: '&aLitebans reloaded. &cFailed to connect to database.'
    reload_fail: '&cReload failed.'
    reload_fail_config: |-
      &c[LiteBans] &4config.yml is not valid and could not be loaded, the default configuration will be used.
      &cPlease check the server console for more information.
    reload_fail_messages: |-
      &c[LiteBans] &4messages.yml is not valid and could not be loaded, default messages will be used.
      &cPlease check the server console for more information.
    add_history_usage: '&c$command addhistory <name> <UUID> <IP>'
    add_history: '&aHistory added.'
    fix_history: '&aFixing history for table $table...'
    fix_history_result: '&aAdded $amount entries.'
    fix_history_offline_uuids: '&c$amount UUIDs were not fetched from Mojang since
      they are offline-mode UUIDs.'
    import_usage: '&c$command import start'
    import_start: '&aImporting from $db, this might take a while...'
    import_finish: '&aImport finished successfully. $bans bans imported, $ipbans IP-bans.'
    import_finish_litebans: '&aImport finished successfully. Added $amount entries
      to the database.'
    import_fail: '&cImport failed. Check console.'
    import_unsupported: '&cImporting from ''$name'' is not supported yet!'
    sqlexec_success: '&aSQL query executed successfully.'
    sqlexec_failure: '&cFailed to execute.'
duration:
  expired: expirováno
  forever: navždy
  year: rok
  years: let
  month: měsíc
  months: měsíců
  week: týden
  weeks: týdnu
  day: den
  days: dnů
  hour: hodinu
  hours: hodin
  minute: minutu
  minutes: minut
  second: sekundu
  seconds: sekund
  format: '%d %s'
  separator: ', '
strings:
  global: globálně
  'null': neurčeno
  'true': ano
  'false': ne
